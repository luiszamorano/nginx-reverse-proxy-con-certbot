import os
import subprocess
import json

# Cargar configuración desde JSON
with open('config.json', 'r') as file:
    config = json.load(file)

email = config.get("email")
dominio = config.get("dominio")
root_destino = config.get("root_destino")
plugin = config.get("plugin")
duckdns_token = config.get("duckdns_token")

paquetes_a_instalar = ['nginx','certbot','certbot-nginx']
for paquete in paquetes_a_instalar:
    print(f'\-------\n INSTALANDO {paquete}----\n\n')
    proceso = subprocess.Popen(['apk', 'add', paquete], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    salida, error = proceso.communicate()
    print(salida)
    print(f'\n-------\n')

# puedes editar esto segun tus necesidades 
# f antes de " es para poner variables dentro del string (forma más facil de concatenar)
configuracion_nginx = [
    "server {",
    f"   server_name {dominio};",
    "   location / {",
    f"       proxy_pass {root_destino};",
    "       proxy_set_header Host $host;",
    "       proxy_set_header X-Real-IP $remote_addr;",
    "       proxy_set_header X-Forwarded-FOR $proxy_add_x_forwarded_for;",
    "       proxy_set_header X-Forwarded-Proto $scheme;",
    "   }",
    "}"
]

nombre_ruta_archivo_configuracion = "/etc/nginx/http.d/configuracion-nginx.conf"

print("\n---------CREANDO EL ARCHIVO DE CONFIGURACION------------\n")
proceso = subprocess.Popen(["touch",nombre_ruta_archivo_configuracion], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
salida, error = proceso.communicate()
print(salida)

print("\n---------COPIANDO LINEAS EN EL ARCHIVO DE CONFIGURACION------------\n")
with open(nombre_ruta_archivo_configuracion, 'w') as archivo:
    for linea in configuracion_nginx:
        archivo.write(linea + "\n")

print("\n---------ARRANCANDO CERTBOT------------\n")
# certbot --nginx -d ${dominio_origen} --email ${email} --agree-tos --non-interactive --no-eff-email
if plugin == "nginx":
    resultado = subprocess.run(["certbot","--nginx","-d",dominio,"--renew","--email",email,"--agree-tos","--non-interactive","--no-eff-email"], capture_output=True, text=True)
elif plugin == "duckdns":
    # instalamos plugin con certificados 
    subprocess.run(["pip","install","certbot_dns_duckdns"])
    # ahora ejecutamos el comando personalizado
    # https://pypi.org/project/certbot-dns-duckdns/
    resultado = subprocess.run(["certbot","--nginx","--non-interactive","--agree-tos","--email",email,"--preferred-challenges",dns,"--authenticator","dns-duckdns","--dns-duckdns-token",duckdns_token,"--dns-duckdns-propagation-seconds","60","-d",dominio], capture_output=True, text=True)
print(resultado.stdout)
print(resultado.stderr)
print('\n-------\n')


print("\n---------RENOVACION CERTIFICADOS AUTOMATICA------------\n")
comando cronetab="echo \"0 0,12 * * * sleep $(awk 'BEGIN{srand(); print int(rand()*(3600+1))}') && certbot renew --nginx --force-renewal -q\" | tee -a /etc/crontabs/root"
resultado= subprocess.run([comando_cronetab.split()])
print(resultado.stdout)
print(resultado.stderr)
print('\n-------\n')

subprocess.run(["rm",".env"])