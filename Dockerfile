FROM alpine:3.19

RUN apk update && apk upgrade
RUN apk add nginx certbot certbot-nginx python3 

COPY config.json .

COPY reverse-proxy.py .

RUN python3 reverse-proxy.py

CMD ["nginx", "-g", "daemon off;"]